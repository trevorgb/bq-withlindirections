"""

    Copyright (c) 2013 William Earley

    This file is part of MtGox Trades Tool (https://bitbucket.org/nitrous/bq).

    MtGox Trades Tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MtGox Trades Tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MtGox Trades Tool.  If not, see <http://www.gnu.org/licenses/>.

"""

currencies = {
	"BTC": 100000000,
	"USD": 100000,
	"GBP": 100000,
	"EUR": 100000,
	"JPY": 1000,
	"AUD": 100000,
	"CAD": 100000,
	"CHF": 100000,
	"CNY": 100000,
	"DKK": 100000,
	"HKD": 100000,
	"PLN": 100000,
	"RUB": 100000,
	"SEK": 1000,
	"SGD": 100000,
	"THB": 100000,
	"NOK": 100000,
	"CZK": 100000,
    "NZD": 100000
}

def tail(f, window=1):
	#http://stackoverflow.com/a/136368/1018314
    BUFSIZ = 1024
    f.seek(0, 2)
    bytes = f.tell()
    size = window
    block = -1
    data = []
    while size > 0 and bytes > 0:
        if (bytes - BUFSIZ > 0):
            # Seek back one whole BUFSIZ
            f.seek(block*BUFSIZ, 2)
            # read BUFFER
            data.append(f.read(BUFSIZ))
        else:
            # file too small, start from begining
            f.seek(0,0)
            # only read what was not read
            data.append(f.read(bytes))
        linesFound = data[-1].count('\n')
        size -= linesFound
        bytes -= BUFSIZ
        block -= 1
    return ''.join(data).splitlines()[-window:]