"""

	Copyright (c) 2013 William Earley

    This file is part of MtGox Trades Tool (https://bitbucket.org/nitrous/bq).

    MtGox Trades Tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MtGox Trades Tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MtGox Trades Tool.  If not, see <http://www.gnu.org/licenses/>.

"""

import Tkinter as tk
import ttk

# Custom name for dropdown
# defaults to module name
name = None

# Default export extension
ext = None

class Widget(ttk.LabelFrame):
	def init_gui(self): pass
	def serialise(self): return {}
	def deserialise(self, serial): pass

# latest should return a tuple, (Tag, Value)
# for a comparison with any database field
class Formatter(object):
	def __init__(self, filename, options={}): pass
	def prepare(self, size, last): pass
	def open(self): pass
	def write(self, i, datum): pass
	def latest(self): return None
	def close(self): pass

def validate(options={}):
	return True