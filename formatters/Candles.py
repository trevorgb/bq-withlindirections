"""

	Copyright (c) 2013 William Earley

    This file is part of MtGox Trades Tool (https://bitbucket.org/nitrous/bq).

    MtGox Trades Tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MtGox Trades Tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MtGox Trades Tool.  If not, see <http://www.gnu.org/licenses/>.

"""

import Tkinter as tk
import ttk
import webbrowser
from dateutil.parser import parse as dt_parse
from calendar import timegm
from datetime import datetime
from _data import currencies, tail
import os
import csv

name = "CSV Candle [Unix]"
ext = ".csv"

class Widget(ttk.LabelFrame):
	def init_gui(self):
		entry_validator = self.register(self.validate_int), '%P'
		entry_invalidator = self.register(self.invalid_entry), '%W'
		valid_opts = {'validate':'focusout', 'validatecommand':entry_validator, 'invalidcommand':entry_invalidator}
		
		url = "https://bitcointalk.org/index.php?topic=239815.0"
		ttk.Label(self, text="A port of WhyDifficult's original:").grid(row=0, columnspan=2, padx=10)
		ttk.Button(self, text="Link", command=lambda :webbrowser.open(url, new=1, autoraise=True)).grid(row=0, column=2)
		
		self.duration = tk.StringVar()
		self.duration.set('3600')
		ttk.Label(self, text="Duration in seconds:").grid(row=2, column=0, sticky='w', padx=10)
		ttk.Entry(self, textvariable=self.duration, **valid_opts).grid(row=2, column=1, columnspan=2, sticky='w')

		self.align = tk.IntVar()
		self.align.set(1)
		ttk.Checkbutton(self, variable=self.align, text="Align data to duration (eg on the hour)").grid(columnspan=3, sticky='w')

		self.ignore = tk.IntVar()
		self.ignore.set(1)
		ttk.Checkbutton(self, variable=self.ignore, text="Don't include missing candle data").grid(columnspan=3, sticky='w')

		self.volume = tk.IntVar()
		self.volume.set(1)
		ttk.Checkbutton(self, variable=self.volume, text="Include volume (satoshis)").grid(columnspan=3, sticky='w')

	def validate_int(self, value):
		try:
			value = int(value)
			return value > 0
		except:
			return False

	def foc_flash(self, widget, count=15):
		widget.focus_force()
		if count < 1:
				widget.selection_range(0, tk.END)
		else:
			if count%3 == 0:
				widget.bell()
				widget.selection_range(0, tk.END)
			else:
				widget.selection_range(0, 0)
			self.after(25, lambda:self.foc_flash(widget, count-1))

	def invalid_entry(self, name):
		widget = self.nametowidget(name)
		widget.focus_set()
		widget.bell()
		self.foc_flash(widget)

	def serialise(self):
		return {
			'duration': int(self.duration.get()),
			'align': self.align.get(),
			'ignore': self.ignore.get(),
			'volume': self.volume.get()
		}

	def deserialise(self, serial):
		self.duration.set(str(serial['duration']))
		self.align.set(serial['align'])
		self.ignore.set(serial['ignore'])
		self.volume.set(serial['volume'])

class Formatter(object):
	header_normal = 'date,open,high,low,close\n'
	header_volume = 'date,open,high,low,close,volume\n'
	buff_size = 1000

	def __init__(self, filename, options):
		self.fn = filename
		self.duration = max(int(options['duration']), 0)
		
		self.align = options['align'] == 1
		self.ignore = options['ignore'] == 1
		self.volume = options['volume'] == 1
		self.header = self.header_volume if self.volume else self.header_normal

		self.trades = []
		self.rows = ""
		self.row_count = 0
		self.last = None
		self.fp = None
		self.current = 0

	def prepare(self, size, last):
		pass

	def open(self):
		fn = self.fn
		latest = self.latest()
		if latest is None:
			self.fp = open(fn, 'w')
			self.fp.write(self.header)
		else:
			self.check_header()
			self.current = get_stamp(latest[1]) + self.duration
			self.fp = open(fn, 'a')

	def flush(self):
		self.fp.write(self.rows)
		self.rows = ""
		self.row_count = 0

	def _calculate(self, c):
		return "%d" % c

	def calculate(self):
		t = self.trades
		c = self.current
		if len(t) == 0:
			if self.ignore or self.last is None:
				return
			price, volume = self.last
			t = [(price, 0)]

		p = [price for price,volume in t]
		v = sum(volume for price,volume in t)
		c = self._calculate(c)

		if self.volume:
			candle = c, p[0], max(p), min(p), p[-1], v
			self.rows += "%s,%f,%f,%f,%f,%d\n" % candle
		else:
			candle = c, p[0], max(p), min(p), p[-1]
			self.rows += "%s,%f,%f,%f,%f\n" % candle
		self.row_count += 1
		
		self.trades = []
		if self.row_count % self.buff_size == 0:
			self.flush()

	def write(self, i, datum):
		date = get_stamp(datum['Date'])
		curr = datum['Currency__']
		factor = currencies[curr]
		price = datum['Price'] / float(factor)
		volume = datum['Amount'] # / 1000000.0

		# Setup candle accumulator
		if i == 0 and self.current == 0:
			if self.align and date % self.duration != 0:
				self.current = self.duration * (1 + date//self.duration)
			else:
				self.current = date

		# ignore data before clean boundary
		if date < self.current:
			return

		# if date outside of range, then flush previous candle(s)
		while date >= self.current+self.duration:
			self.calculate()
			self.current += self.duration

		# otherwise, accumulate new prices
		c = self.last = price, volume
		self.trades += [c]

	def _latest(self, last):
		dt = datetime.utcfromtimestamp(float(last[0]))
		return dt.strftime('%Y-%m-%d %H:%M:%S')

	def latest(self):
		fn = self.fn
		if os.path.exists(fn):
			with open(fn, 'r') as fp:
				last = tail(fp)
			if len(last) == 0:
				return None

			try:
				last = list(csv.reader(last, skipinitialspace=True))[0]
			except:
				raise IOError, "Invalid CSV file"

			if last[0][:4] == 'date':
				return None
			try:
				return 'Date', self._latest(last)
			except:
				self.check_header()
				raise IOError, "Corrupt CSV file %s. Check your export settings, if they're correct you need to delete the file and start again." % self.fn
		else:
			return None

	def check_header(self):
		with open(self.fn, 'r') as fp:
			first = fp.readline().strip().lower()
			header = self.header.strip().lower()
			if first != header:
				raise IOError, "Incompatible CSV file - make sure you're using the same export settings as you used to create %s originally." % self.fn

	def close(self):
		if self.fp is not None:
			self.flush()
			self.fp.close()

def validate(options):
	valid = True
	valid = valid and type(options['duration']) is int
	valid = valid and options['duration'] > 0
	return valid

def get_stamp(date):
	return timegm(dt_parse(date).timetuple())