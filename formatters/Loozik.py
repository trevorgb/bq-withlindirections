"""

	Copyright (c) 2013 William Earley

    This file is part of MtGox Trades Tool (https://bitbucket.org/nitrous/bq).

    MtGox Trades Tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MtGox Trades Tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MtGox Trades Tool.  If not, see <http://www.gnu.org/licenses/>.

"""

import Tkinter as tk
import ttk
import os
import csv
from _data import currencies, tail

name = "CSV (price/volume) [ISO]"
ext = ".csv"

'''
This module is named Loozik after the user who requested ISO-formatted MtGox data.

Date,Time,Price ($),Volume (satoshis)
2013-07-05,21:06:30,69.47,100000000
'''

class Widget(ttk.LabelFrame):
	def init_gui(self): pass
	def serialise(self): return {}
	def deserialise(self, serial): pass

class Formatter(object):
	header = 'Date,Time,Price,Volume\n'

	def __init__(self, filename, options={}):
		self.fn = filename
		self.fp = None

	def prepare(self, size, last):
		self.last = last['Date'].strip()

	def open(self):
		fn = self.fn
		if self.latest() is None:
			self.fp = open(fn, 'w')
			self.fp.write(self.header)
		else:
			self.check_header()
			self.fp = open(fn, 'a')

	def write(self, i, datum):
		dt = datum['Date'].strip()
		if dt == self.last:
			return
		date = dt[0:10]
		time = dt[11:19]

		curr = datum['Currency__']
		factor = currencies[curr]
		price = float(datum['Price']) / factor
		volume = int(datum['Amount'])

		line = "%s,%s,%f,%d\n" % (date,time,price,volume)
		self.fp.write(line)

		if i%10000 == 0:
			self.fp.flush()

	def latest(self):
		fn = self.fn
		if os.path.exists(fn):
			with open(fn, 'r') as fp:
				last = tail(fp)
			if len(last) == 0:
				return None

			try:
				last = list(csv.reader(last, skipinitialspace=True))[0]
			except:
				raise IOError, "Invalid CSV file"

			if last[0][:4] == 'Date':
				return None
			return 'Date', "%s %s" % tuple(last[:2])
		else:
			return None

	def check_header(self):
		with open(self.fn, 'r') as fp:
			first = fp.readline().strip().lower()
			header = self.header.strip().lower()
			if first != header:
				raise IOError, "Incompatible CSV file - make sure you're using the same export settings as you used to create %s originally" % self.fn

	def close(self):
		if self.fp is not None:
			self.fp.close()

def validate(options={}):
	return True