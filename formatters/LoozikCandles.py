"""

	Copyright (c) 2013 William Earley

    This file is part of MtGox Trades Tool (https://bitbucket.org/nitrous/bq).

    MtGox Trades Tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MtGox Trades Tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MtGox Trades Tool.  If not, see <http://www.gnu.org/licenses/>.

"""

from datetime import datetime
import Candles

name = "CSV Candle [ISO]"
ext = ".csv"

'''
This module is named Loozik after the user who requested ISO-formatted MtGox data.
It is a version of Candles.py but using ISO dates instead of Unix timestamps.
'''

class Widget(Candles.Widget):
	pass

class Formatter(Candles.Formatter):
	header_normal = 'date,time,open,high,low,close\n'
	header_volume = 'date,time,open,high,low,close,volume\n'

	def _calculate(self, c):
		dt = datetime.utcfromtimestamp(c)
		cd = dt.strftime('%Y-%m-%d')
		ct = dt.strftime('%H:%M:%S')
		return "%s,%s" % (cd,ct)

	def _latest(self, last):
		return "%s %s" % tuple(last[:2])