"""

	Copyright (c) 2013 William Earley

    This file is part of MtGox Trades Tool (https://bitbucket.org/nitrous/bq).

    MtGox Trades Tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MtGox Trades Tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MtGox Trades Tool.  If not, see <http://www.gnu.org/licenses/>.

"""

import Tkinter as tk
import ttk
import webbrowser
import os
import sqlite3
from dateutil.parser import parse as dt_parse
from calendar import timegm

name = "SQLite3 PhantomCircuit"
ext = ".sqlite3"

class Widget(ttk.LabelFrame):
	def init_gui(self):
		url = "http://cahier2.ww7.be/bitcoinmirror/phantomcircuit/"
		ttk.Label(self, text="A port of PhantomCircuit's original:").pack()
		ttk.Button(self, text="Link", command=lambda :webbrowser.open(url, new=1, autoraise=True)).pack()

	def serialise(self): return {}
	def deserialise(self, serial): pass

class Formatter(object):
	buff_size = 10000

	def __init__(self, filename, options={}):
		self.fn = filename
		self.db = None
		self.buffer = []

	def prepare(self, size, last):
		pass

	def open(self):
		# Make sure is valid db
		self.latest()
		db = self.db = sqlite3.connect(self.fn)
		
		try:
			db.execute('PRAGMA checkpoint_fullfsync=false')
			db.execute('PRAGMA fullfsync=false')
			db.execute('PRAGMA journal_mode=WAL')
			db.execute('PRAGMA synchronous=off')
			db.execute('PRAGMA temp_store=MEMORY')

			db.execute('CREATE TABLE IF NOT EXISTS trades(tid integer,currency text,amount real,price real,date integer,real boolean);')
			db.execute('CREATE UNIQUE INDEX IF NOT EXISTS trades_currency_tid_index on trades(currency,tid);')
			self.cursor = db.cursor()
		except:
			db.close()
			raise IOError, "Couldn't open database"

	def flush(self, cont=True):
		self.cursor.executemany("""
		INSERT OR IGNORE INTO trades(tid,currency,amount,price,date,real)
		VALUES (?,?,?,?,?,?)
		""", self.buffer)
		self.buffer = []

		self.db.commit()
		self.cursor.close()
		if cont:
			self.cursor = self.db.cursor()

	def write(self, i, datum):
		date = timegm(dt_parse(datum['Date']).timetuple())
		vals = datum['Money_Trade__'], datum['Currency__'], datum['Amount'], datum['Price'], date, datum['Primary']=='true'
		self.buffer += [vals]

		# buffer every 10,000
		if (i+1)%self.buff_size == 0:
			self.flush()

	def latest(self):
		if os.path.exists(self.fn):
			try:
				db = sqlite3.connect(self.fn)
			except:
				raise IOError("Invalid or corrupt database")

			result = None
			try:
				cursor = db.cursor()
				cursor.execute("select max(tid) from trades")
				result = "Money_Trade__", int(cursor.fetchone()[0])
			except:
				pass
			finally:
				db.close()
				return result
		return None

	def close(self):
		if self.db is not None:
			self.flush(False)
			self.db.close()
			self.db = self.cursor = None

def validate(options={}):
	return True