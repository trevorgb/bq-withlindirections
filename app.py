#!/usr/bin/env python
"""

	Copyright (c) 2013 William Earley

    This file is part of MtGox Trades Tool (https://bitbucket.org/nitrous/bq).

    MtGox Trades Tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MtGox Trades Tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MtGox Trades Tool.  If not, see <http://www.gnu.org/licenses/>.

"""

name = "MtGox-Trades-Tool"
author = "Nitrous"
version = "1.0"

clid='332124338208-lb4qdq03ugb9trrtoen4jois45discju.apps.googleusercontent.com'
clsc='6Obfbhe-93FB7VqJKvIHA4F-'

if __name__ == '__main__':
	import os
	import sys
	import errno
	import appdirs
	datadir = appdirs.user_data_dir(name, author)

	try:
		os.makedirs(datadir)
	except OSError as exc:
		if exc.errno == errno.EEXIST and os.path.isdir(datadir):
			pass
		else:
			raise

	def module_path():
		# py2exe doesn't support __file__
		try:
			return os.path.dirname(__file__)
		except:
			if hasattr(sys, "frozen"):
				return os.path.dirname(unicode(sys.executable, sys.getfilesystemencoding()))
			return os.path.dirname(unicode(__file__, sys.getfilesystemencoding()))

	import Tkinter as tk
	import tkMessageBox
	import webbrowser
	import traceback
	import threading
	import time

	fatal_lock = threading.Lock()
	fatal_error = None
	fatal_hard_exit = False

	def fatal(tb, hard_exit=False, thread=False):
		if thread:
			global fatal_lock
			with fatal_lock:
				global fatal_error, fatal_hard_exit
				if fatal_error is None:
					fatal_error = tb
					fatal_hard_exit = hard_exit
			while True:
				time.sleep(1)
				
		url = "https://bitcointalk.org/index.php?topic=221055.0"
		tkMessageBox.showerror("Unexpected Exception", "An error occurred that shouldn't have happened. "
				"Please report this on the tool's forum thread at bitcointalk (you should be taken there when "
				"you click ok).\n\n%s" % tb)
		webbrowser.open(url, new=1, autoraise=True)

		try:
			sys.stderr.write(tb)
		finally:
			os._exit(1) #induce crash

			# py2app doesn't include os.exit!!!
			if hard_exit:
				os._exit(1)
			else:
				os.exit(1)

	class Catcher: 
		def __init__(self, func, subst, widget):
			self.func = func 
			self.subst = subst
			self.widget = widget
		def __call__(self, *args):
			try:
				if self.subst:
					args = apply(self.subst, args)
				return apply(self.func, args)
			except SystemExit, msg:
				raise SystemExit, msg
			except:
				fatal(traceback.format_exc())
				raise

	OriginalThread = threading.Thread
	class ThreadCatcher(OriginalThread):
		def __init__(self, **kwargs):
			OriginalThread.__init__(self, **kwargs)
			self.thread_init = self.run
			self.run = self.thread_bootstrap

		def thread_bootstrap(self):
			try:
				self.thread_init()
			except:
				fatal(traceback.format_exc(), True, True)
				os._exit(1)

	tk.CallWrapper = Catcher
	threading.Thread = ThreadCatcher

	def errands():
		global fatal_lock
		with fatal_lock:
			global fatal_error, fatal_hard_exit
			if fatal_error is not None:
				fatal(fatal_error, fatal_hard_exit)




	import gui, bq

	bq.ca_certs = os.path.join(module_path(), 'httplib2', 'cacerts.txt')
	store = os.path.join(datadir, 'creds.dat')
	gui.run(store=store, clid=clid, clsc=clsc, cb=errands)