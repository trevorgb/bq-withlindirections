#!/usr/bin/env python
"""

    Copyright (c) 2013 William Earley

    This file is part of MtGox Trades Tool (https://bitbucket.org/nitrous/bq).

    MtGox Trades Tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MtGox Trades Tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MtGox Trades Tool.  If not, see <http://www.gnu.org/licenses/>.

"""

import Tkinter as tk
import ttk
import tkFileDialog
import tkMessageBox
import time
import threading
from mtgox import Updater
from pykrete import DB, Tag, Value
from export import ExportWindow
import bq
import os
import platform
import traceback

def is_valid_sqlite3(fn):
    if not os.path.isfile(fn):
        # if it doesn't exist, we can write it
        return not os.path.exists(fn)
    size = os.path.getsize(fn)
    if size == 0:
        return True # empty db
    elif 0 < size < 100: # SQLite database file header is 100 bytes
        return False
    else:
        with open(fn, 'rb') as fp:
            header = fp.read(100)
        return header[0:16] == 'SQLite format 3\000'

class Application(ttk.Frame):
    def __init__( self, parent, bqo, **opt ):
        ttk.Frame.__init__( self, parent, **opt )
        self.parent = parent
        parent.protocol('WM_DELETE_WINDOW', self.safe_quit)
        self.bqo = bqo
        if platform.system().lower() == 'darwin':
            self.modifier = 'Command'
        else:
            self.modifier = 'Control'
        parent.createcommand('exit', self.safe_quit)
        self.init_gui()
        self.dump = ""
        self.export_win = None
        self.thread = None

    def init_gui( self ):
        self.parent.title( "MtGox Trades Tool" )

        # Create menu bar
        self.menubar = tk.Menu(self)
        self.parent.config(menu=self.menubar)

        # File menu : load new | close
        self.menu_file = menu = tk.Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label='File', menu=menu)
        menu.add_command(label='Load Dump...', command=self.open_dump, accelerator="%s+o"%self.modifier)
        menu.add_command(label='New Dump...', command=self.save_dump, accelerator="%s+n"%self.modifier)
        menu.add_separator()
        menu.add_command(label='Export As...', command=self.export_as, accelerator="%s+e"%self.modifier)
        menu.add_command(label='Close', command=self.safe_quit, accelerator="%s+w"%self.modifier)
        self.parent.bind("<%s-o>"%self.modifier, self.open_dump)
        self.parent.bind("<%s-n>"%self.modifier, self.save_dump)
        self.parent.bind("<%s-e>"%self.modifier, self.export_as)
        self.parent.bind("<%s-w>"%self.modifier, self.safe_quit)

        # Create the stats frame
        self.stats = ttk.Frame( self, borderwidth = 0 )
        self.stats.pack( fill = tk.BOTH, expand = 1 )
        ttk.Label(self.stats, text='Dump DB path:').grid(sticky=tk.W)
        ttk.Label(self.stats, text='Rows downloaded:').grid(sticky=tk.W)
        ttk.Label(self.stats, text='Latest TID:').grid(sticky=tk.W)
        ttk.Label(self.stats, text='Data up to:').grid(sticky=tk.W)
        self.stats_path = ttk.Label(self.stats)
        self.stats_rows = ttk.Label(self.stats)
        self.stats_utid = ttk.Label(self.stats)
        self.stats_date = ttk.Label(self.stats)
        self.stats_path.grid(row=0, column=1, sticky=tk.W)
        self.stats_rows.grid(row=1, column=1, sticky=tk.W)
        self.stats_utid.grid(row=2, column=1, sticky=tk.W)
        self.stats_date.grid(row=3, column=1, sticky=tk.W)

        # Create the frame above the buttons
        self.frame = ttk.Frame( self, relief = tk.FLAT, borderwidth = 1, padding = 3 )
        self.frame.pack( fill = tk.BOTH, expand = 1 )
        self.pack( fill = tk.BOTH, expand = 1 )

        # Add a log box
        self.log = tk.Listbox( self.frame, relief = tk.SUNKEN, borderwidth = 1, height = 5 )
        self.log.pack( fill = tk.BOTH, expand = 1 )
        
        # Add a progress bar
        self.progress_amt = tk.IntVar()
        self.progress = ttk.Progressbar( self, variable = self.progress_amt )
        self.progress.pack( fill = tk.BOTH, padx = 5, pady = 5 )

        ttk.Separator(self).pack(fill=tk.BOTH,expand=0)

        # Add the buttons to the bottom right
        self.quit_button_config = {'text':'Quit', 'command':self.safe_quit}
        self.cancel_button_config = {'text':'Cancel', 'command':self.cancel_update}
        self.quit_button = ttk.Button( self, width = 10, **self.quit_button_config )
        self.quit_button.pack( side = tk.RIGHT, padx = 5, pady = 5 )
        self.update_button = ttk.Button( self, text = "Update", width = 10, command = self.begin_update, state = tk.DISABLED )
        self.update_button.pack( side = tk.RIGHT )

    def open_dump(self, *e):
        self.load_dump(tkFileDialog.askopenfilename(parent=self))

    def save_dump(self, *e):
        self.load_dump(tkFileDialog.asksaveasfilename(defaultextension='sql', initialfile='dump.sql', parent=self))

    def load_dump(self, path):
        '''

        If path doesn't exist, load
        If path does exist but is not a file/readable, Error
        If file is empty, load
        If filesize between [0,100], corrupt/not SQLite3, Error
        Otherwise, try to get stats and show
        Finally load

        '''
        if path.strip() == "":
            # dialog cancelled
            return

        # if path doesn't exist, fine to create
        # else check the path is ok and accessible
        if os.path.exists(path):
            # make sure it's a file and readable
            try:
                fp = open(path, 'r')
                fp.read(1)
                fp.close()
            except IOError:
                self.log_error("Path is not a file or cannot be read")
                return
        
            # make sure it's a valid database
            size = os.path.getsize(path)
            if size == 0:
                pass
            elif 0 < size < 100:
                self.log_error("Invalid or corrupt SQLite3 database")
                return
            else:
                try:
                    with open(path, 'rb') as fp:
                        header = fp.read(100)
                except IOError:
                    self.log_error("Path inaccessible")
                    return
                if header[0:16] != 'SQLite format 3\000':
                    self.log_error("Invalid or corrupt SQLite3 database")
                    return

            # try to open it
            try:
                db = DB(path)
            except:
                self.log_error("Can't open database")
                return

            # try to get stats
            try:
                dump = db['dump']
                rows = len(dump)
                utid = list(db('SELECT MAX(Money_Trade__) FROM dump'))[0][0]
                date = list(dump('date', Money_Trade__=utid))[0][0]
                self.dump_stats(rows, utid, date)
            except (IndexError, KeyError):
                # table or rows don't exist yet
                self.dump_stats()
                pass
            except:
                # unexpected exception
                self.log_error("Unexpected error for database:\n%s" % traceback.format_exc())
                return
            finally:
                db.close()
        else:
            # need to make sure file is writable
            # eg checking valid permissions, etc
            try:
                with open(path, 'w') as fp:
                    fp.write('Testing')
                os.remove(path)
            except (IOError, OSError):
                self.log_error("Couldn't open file for writing, check your permissions")
                return

        # path has passed the test
        self.stats_path.config(text=path)
        self.dump = path
        self.update_button.config(state=tk.NORMAL)
        self.update()

    def dump_stats(self, rows='', utid='', date='', thread=True):
        if thread:
            self.after(1, lambda:self.dump_stats(rows, utid, date, False))
            return

        self.stats_path.config(text=self.dump)
        self.stats_rows.config(text=str(rows))
        self.stats_utid.config(text=str(utid))
        self.stats_date.config(text=date)

    def begin_update( self ):
        if self.thread is not None:
            self.update_button.config(state=tk.DISABLED)
            return
        if self.dump.strip() == "":
            self.log_error("Invalid dump DB path")
            return
        # Begin the update here in a seperate thread
        self.update_button.config( state = tk.DISABLED )
        self.quit_button.config( **self.cancel_button_config )
        t = Updater()
        self.thread = t
        t.setup(self, self.bqo, self.dump, self.dump_stats)
        t.start()

    def cancel_update( self, quit=False ):
        quit = quit is True
        if self.thread is not None:
            self.thread.cancel(quit=quit)
            self.quit_button.config( state = tk.DISABLED )

    def finish_update( self, thread=True ):
        if thread:
            self.after(1, lambda:self.finish_update(False))
            return
        # This is called by the update thread when the download is complete
        self.update_button.config( state = tk.NORMAL )
        self.quit_button.config( state = tk.NORMAL, **self.quit_button_config )
        self.update_progress( 0.0 )
        self.thread = None

    def update_progress( self, percentage, thread=True ):
        if thread:
            self.after(1, lambda:self.update_progress(percentage, False))
            return

        assert( 0.0 <= percentage <= 100.0 )
        self.progress_amt.set( percentage )

    def export_as( self, *e ):
        if self.thread is None and self.export_win is None and self.dump.strip() != "":
            self.update_button.config(state = tk.DISABLED)
            self.export_win = ExportWindow(self.parent, self, self.dump, self.export_close)
            self.export_win.focus_set()
            self.export_win.grab_set()
            self.export_win.transient(self.parent)
        else:
            self.log_error("You must have loaded a dump and not be currently updating in order to export.")

    def export_close(self):
        if self.export_win is not None:
            self.export_win = None
            self.update_button.config(state = tk.NORMAL)

    def log_error( self, message ):
        tkMessageBox.showerror("Error", message)

    def log_output( self, message, thread=True ):
        if thread:
            self.after(1, lambda:self.log_output(message, False))
            return

        self.log.select_clear( self.log.size() - 2 )
        self.log.insert( tk.END, message )
        self.log.yview( tk.END )

    def safe_quit( self, *e ):
        if self.thread is not None:
            self.log_output("Quitting...")
            self.cancel_update(quit=True)
        else:
            if self.export_win is not None:
                self.log_output("Cancelling export...")
                self.export_win.cancel_export(quit=True)
            else:
                self.quit()

def app_errands(root, cb, every=20):
    cb()
    root.after(every, lambda:app_errands(root,cb,every))


def run(store='creds.dat', clid=None, clsc=None, cb=None):
    bqo = bq.gui(clid, clsc, store=store)
    if not bqo.authenticated():
        tkMessageBox.showerror("Google Error", "Couldn't authenticate with Google BigQuery")
        return

    root = tk.Tk()
    root.resizable(width=False, height=False)
    root.minsize(480, 0)
    app = Application(root, bqo)
    if cb is not None:
        app_errands(app, cb)
    root.mainloop()

if __name__ == '__main__':
    run()