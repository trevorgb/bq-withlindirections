"""

	Copyright (c) 2013 William Earley

    This file is part of MtGox Trades Tool (https://bitbucket.org/nitrous/bq).

    MtGox Trades Tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MtGox Trades Tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MtGox Trades Tool.  If not, see <http://www.gnu.org/licenses/>.

"""

import Tkinter as tk
import ttk
import tkMessageBox
import webbrowser
import time

from httplib2 import Http
from apiclient.discovery import build
from oauth2client.file import Storage
from oauth2client.client import flow_from_clientsecrets, OAuth2WebServerFlow, FlowExchangeError
from oauth2client.tools import run

ca_certs = None

class bq(object):
	def __init__(self, sec='client_secrets.json', proj=None, store='creds.dat'):
		global ca_certs

		storage = Storage(store)
		credentials = storage.get()

		if credentials is None or credentials.invalid:
			scope = 'https://www.googleapis.com/auth/bigquery'
			flow = flow_from_clientsecrets(sec, scope=scope)
			credentials = run(flow, storage)

		_proj = credentials.client_id.split('.', 1)[0]
		self.proj = _proj if proj is None else proj

		try:
			with open(ca_certs, 'r'): pass
			http = credentials.authorize(Http(ca_certs=ca_certs))
		except IOError:
			http = credentials.authorize(Http())

		self.service = build('bigquery', 'v2', http=http)

	# no exception catching, up to user/caller to handle appropriately
	def query(self, q, timeout=0):
		req = self.service.jobs()
		params = {'projectId': self.proj}
		resp = req.query(body={'query': q}, **params).execute()
		job = resp['jobReference']

		params['jobId'] = job['jobId']
		params['maxResults'] = 100000
		while not resp['jobComplete']:
			resp = req.getQueryResults(timeoutMs=timeout, **params).execute()

		schema = resp['schema']
		fields = [f['name'] for f in schema['fields']]

		def gen(resp):
			pos = 0
			total = int(resp['totalRows'])

			while 'rows' in resp and pos < total:
				print '. %d %d %d' % (pos, total, len(resp['rows']))
				rows = resp['rows']
				for row in rows:
					row = [x['v'] for x in row['f']]
					yield dict(zip(fields, row))
				pos += len(rows)
				resp = req.getQueryResults(startIndex=pos, **params).execute()

		return {'schema': schema, 'fields': fields, 'rows': gen(resp) if 'rows' in resp else []}

	def schema(self, proj, data, table):
		req = self.service.tables()
		params = {'projectId': proj, 'datasetId': data, 'tableId': table}
		return req.get(**params).execute()['schema']

	def length(self, proj, data, table):
		req = self.service.tabledata()
		params = {'projectId': proj, 'datasetId': data, 'tableId': table, 'maxResults': 1}
		return long(req.list(**params).execute()['totalRows'])

	def table(self, proj, data, table, start=0, perpage=16000):
		req = self.service.tabledata()
		params = {'projectId': proj, 'datasetId': data, 'tableId': table, 'maxResults': perpage}
		schema = self.schema(proj, data, table)
		fields = [f['name'] for f in schema['fields']]

		def gen(req):
			token = None
			while True:
				if token is None:
					resp = req.list(startIndex=start, **params).execute()
				else:
					resp = req.list(pageToken=token, **params).execute()

				if 'rows' in resp:
					token = resp['pageToken']
					for row in resp['rows']:
						row = [x['v'] for x in row['f']]
						yield dict(zip(fields, row))
				else:
					break

		def gen2(req):
			token = None
			while True:
				if token is None:
					resp = req.list(startIndex=start, **params).execute()
				else:
					resp = req.list(pageToken=token, **params).execute()

				if 'rows' in resp:
					token = resp['pageToken']
					yield [dict(zip(fields, [x['v'] for x in row['f']])) for row in resp['rows']]
				else:
					break

		return {'schema': schema, 'fields': fields, 'rows': gen(req), 'pages': gen2(req)}

	def display(self, q):
		data = self.query(q)
		fields = data['fields']
		print '\t'.join(fields)
		print '\t'.join('-'*len(f) for f in fields)
		for row in data['rows']:
			print '\t'.join(str(row[f]) for f in fields)

class gui(bq):
	def __init__(self, clid, clsc, proj=None, store='creds.dat'):
		storage = Storage(store)
		credentials = storage.get()
		self.service = None
		self.proj = proj

		if credentials is None or credentials.invalid:
			scope = 'https://www.googleapis.com/auth/bigquery'
			self.authenticate(storage, clid, clsc, scope)
		self.complete(storage.get())

	def authenticate(self, storage, clid, clsc, scope):
		flow = OAuth2WebServerFlow(clid, clsc, scope)
		flow.redirect_uri = 'urn:ietf:wg:oauth:2.0:oob'
		url = flow.step1_get_authorize_url()

		root = tk.Tk()
		root.title("OAuth2 Authentication")
		#win.geometry("480x240")
		root.resizable(width=False, height=False)

		frame = ttk.Frame(root)
		frame.pack()

		text = ""
		text = "A web browser has been opened to authorise your usage of this program. If not, click the button to copy the url, then paste it in your web browser:"
		ttk.Label(frame, text=text, wraplength=480, justify='left').grid(row=0, columnspan=2, padx=20, pady=10)

		copybox = tk.Text()
		def copyurl(*e):
			copybox.clipboard_clear()
			copybox.clipboard_append(url)

		ttk.Button(frame, text='Copy URL', command=copyurl).grid(row=1, columnspan=2, pady=10)

		text = "Follow the instructions. You should receive a code, paste it below:"
		ttk.Label(frame, text=text, wraplength=480, justify='left').grid(row=2, columnspan=2, padx=20, pady=10)

		codebox = ttk.Entry(frame)
		codevar = tk.StringVar()
		codebox.config(textvariable=codevar)
		codebox.bind('<ButtonRelease-1>', lambda *e:codebox.select_range(0,tk.END))
		codebox.grid(row=3, pady=10)

		def complete(*e):
			global ca_certs

			try:
				with open(ca_certs, 'r'): pass
				http = Http(ca_certs=ca_certs)
			except IOError:
				http = Http()

			code = codevar.get().strip()
			try:
				credential = flow.step2_exchange(code, http)
			except FlowExchangeError:
				msg = "The code you entered was invalid. Please make sure you entered the code correctly, or try the authentication process again."
				tkMessageBox.showerror("OAuth2 Error", msg)
				codevar.set("")
				return
			storage.put(credential)
			credential.set_store(storage)
			root.destroy()

		ttk.Button(frame, text="Complete Authentication", command=complete).grid(row=3, column=1, pady=10)

		try:
			webbrowser.open(url, new=1, autoraise=True)
		except webbrowser.Error:
			pass
		root.mainloop()

	def complete(self, credentials):
		global ca_certs

		proj = self.proj
		_proj = credentials.client_id.split('.', 1)[0]
		self.proj = _proj if proj is None else proj

		t0 = time.time()
		while self.service is None:
			if time.time() - t0 > 30:
				tkMessageBox.showerror("Google Error", "Google's BigQuery service timed out! Please try again later.")
				return
			try:
				with open(ca_certs, 'r'): pass
				http = credentials.authorize(Http(ca_certs=ca_certs))
			except IOError:
				http = credentials.authorize(Http())
			self.service = build('bigquery', 'v2', http=http)

	def authenticated(self):
		return self.service is not None