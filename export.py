"""

    Copyright (c) 2013 William Earley

    This file is part of MtGox Trades Tool (https://bitbucket.org/nitrous/bq).

    MtGox Trades Tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MtGox Trades Tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MtGox Trades Tool.  If not, see <http://www.gnu.org/licenses/>.

"""

import Tkinter as tk
import ttk
import tkMessageBox
import tkFileDialog
import platform
import webbrowser
from calendar import timegm
from datetime import datetime
from dateutil.parser import parse as dt_parse
from mtgox import Exporter
import pykrete
import formatters
import json
import os
import math
import operator
import threading

currencies = ['*', 'USD', 'AUD', 'CAD', 'CHF', 'CNY', 'DKK', 'EUR', 'GBP', 'HKD', 'JPY', 'NZD', 'PLN', 'RUB', 'SEK', 'SGD', 'THB', 'NOK', 'CZK']

db_schema = """The dump is a SQLite3 database containing a table named dump and the following fields:

Money_Trade__, Primary, Currency__, Bid_User_Rest_App__, Ask_User_Rest_App__, Type, Properties, Item, Amount, Price, Date

Your query should in most cases select all of these fields and let the formatter filter these itself, and should definitely select the Money_Trade__ field. You probably want to sort by Money_Trade__ ascending as well. Please note that these queries can take a little bit longer to perform.

Available currencies are %s.

***** Be VERY careful not to make any changes to the database with your query. You should only perform SELECT statements. If you modify the database your exports may be silently incomplete or corrupted and to fix it you will need to redownload the entire dump! *****

If you need more information, click yes to be taken to a bitcointalk forum thread with a table of these fields, their descriptions, and other relevant information about the MtGox bigquery database.""" % ', '.join(currencies[1:])

simple_help = """Here you need to specify what data to export.

The Currency dropdown allows you to select which currency's trades to include, or you can include all of them with *.

The Start and End textboxes allow you to enter the date range. Leave either blank to have it unbounded, or enter a date-time format in the box. The timezone is GMT/UTC by default, but you can specify your own timezone offset. It is recommended that you use the ISO 8601 format, YYYY-MM-DDTHH:MM:SSZ (others are supported but you may get unexpected behaviour).

"2013-06-07T15:07:30Z" gives a timestamp of 1370617650.
"2013-06-07T15:07:30Z-0400" (EDT) gives a timestamp of 1370603250.

If you leave the start blank, then all data up to the end date will be included. If you leave the end blank, all data from the start will be included. If you leave both blank, all data will be included. The start date must be before the end date.

Some trades appear in other order books than their original currencies, to eliminate these duplicates you should leave the non-primary checkbox blank."""

def replace_text(widget, text, index=1.0):
    widget.delete(index, tk.END)
    widget.insert(index, text)

class ExportWindow(tk.Toplevel):
    def __init__(self, parent=None, app=None, database=None, on_close=lambda *x:None, **opt):
        tk.Toplevel.__init__(self, parent, **opt)
        self.title('Export Data')
        self.resizable(width=False, height=False)
        self.minsize(360, 0)
        
        self.database = database
        self.app = app
        self.parent = parent
        self.on_close = on_close
        self.protocol('WM_DELETE_WINDOW', self.cancel_export)
        if platform.system().lower() == 'darwin':
            self.modifier = 'Command'
        else:
            self.modifier = 'Control'
        if platform.system().lower().startswith('win'):
            self.update_progress = self.update_progress2
            tk.Label(self, text="The progress bar has been disabled on Windows", fg="red").pack(fill=tk.X, expand=1)
        self.thread = None
        self.recc_ext = None
        
        self.frame = ttk.Frame(self)
        self.frame.pack(fill=tk.BOTH, expand=1)
        self.init_gui()
        self.bind("<Escape>", self.cancel_export)
        self.perc = 0.0
        self.lock = threading.Lock()

    def init_gui(self):
        global currencies
        self.frame.grid_columnconfigure(0, minsize=360)

        # preference load and save buttons
        self.pref_frame = ttk.Frame(self.frame)
        ttk.Button(self.pref_frame, text='Load', command=self.load_prefs).pack(side=tk.LEFT)
        ttk.Button(self.pref_frame, text='Save', command=self.save_prefs).pack()
        self.pref_frame.grid(row=0, sticky='e', padx=5, pady=5)

        ttk.Separator(self.frame).grid(row=1, sticky=tk.E+tk.W)

        # data selection notebook
        ttk.Label(self.frame, text='Choose the data range to be exported:').grid(row=2, sticky='w', padx=3, pady=5)

        self.data_sel = ttk.Notebook(self.frame)
        if ttk.Style().theme_use() == 'aqua':
            self.data_sel.grid(row=3, sticky=tk.W+tk.E)
        else:
            self.data_sel.grid(row=3, sticky=tk.W+tk.E, padx=16)

        self.data_simple = ttk.Frame(self.data_sel)
        self.data_sel.add(self.data_simple, text='Simple', sticky=tk.W+tk.E)
        ttk.Label(self.data_simple, text='Currency:').grid(row=0, column=0)
        self.simp_currencies = ttk.Combobox(self.data_simple, state='readonly', exportselection=0)
        self.simp_currencies.config(values=currencies)
        self.simp_currencies.set('*')
        self.simp_currencies.bind("<<ComboboxSelected>>", lambda e:None)
        self.simp_currencies.grid(row=0, column=1)
        ttk.Button(self.data_simple, text='Help', width=4, command=self.simp_help).grid(row=0, column=2)
        date_validator = self.register(self.validate_date), '%P'
        entry_invalidator = self.register(self.invalid_entry), '%W', '%s'
        valid_opts = {'validate':'focusout', 'validatecommand':date_validator, 'invalidcommand':entry_invalidator}
        ttk.Label(self.data_simple, text='Start:').grid(row=1, column=0)
        self.simp_start = ttk.Entry(self.data_simple, **valid_opts)
        self.simp_start.grid(row=1, column=1)
        ttk.Label(self.data_simple, text='End:').grid(row=2, column=0)
        self.simp_end = ttk.Entry(self.data_simple, **valid_opts)
        self.simp_end.grid(row=2, column=1)
        self.simp_primary_value = tk.IntVar()
        self.simp_primary_value.set(0)
        self.simp_primary = ttk.Checkbutton(self.data_simple, variable=self.simp_primary_value, text='Include non-primary trades')
        self.simp_primary.grid(row=3, columnspan=2)

        self.data_adv = ttk.Frame(self.data_sel)
        self.data_sel.add(self.data_adv, text='Advanced')
        ttk.Label(self.data_adv, text='SQL Command:').grid(row=0, column=0, sticky='w')
        ttk.Button(self.data_adv, text='Database Schema', command=self.show_schema).grid(row=0, column=1, sticky='e')
        self.adv_sql = tk.Text(self.data_adv, width=45, height=5)
        self.adv_sql.grid(row=1, columnspan=2, sticky=tk.E+tk.W)

        # formatter dropdown
        ttk.Separator(self.frame).grid(row=4, sticky=tk.E+tk.W)

        self.format_frame = ttk.Frame(self.frame)
        ttk.Label(self.format_frame, text='Format:').grid(sticky='w')
        self.format_sel = ttk.Combobox(self.format_frame, state='readonly', exportselection=0)
        self.configure_formatters()
        self.format_sel.bind("<<ComboboxSelected>>", self.select_format)
        self.format_sel.grid(row=0, column=1, columnspan=2, sticky='w')
        ttk.Label(self.format_frame, text='Export As:').grid(sticky='w')
        self.xfile_value=tk.StringVar()
        ttk.Entry(self.format_frame, textvariable=self.xfile_value, state='readonly').grid(row=1, column=1, sticky='w')
        ttk.Button(self.format_frame, text='Browse...', command=self.export_as).grid(row=1, column=2, sticky='w')
        self.options_frame = ttk.Frame(self.format_frame)
        self.options_frame.grid(row=2, columnspan=3)
        self.format_frame.grid(row=5, padx=5, pady=5)

        self.progress_amt = tk.IntVar()
        self.progress = ttk.Progressbar(self.frame, variable=self.progress_amt)
        self.progress.grid(row=6, sticky=tk.E+tk.W, padx=5)

        # export and cancel buttons
        ttk.Separator(self.frame).grid(row=7, sticky=tk.E+tk.W)

        self.action_frame = ttk.Frame(self.frame)
        self.export_btn = ttk.Button(self.action_frame, text='Export', command=self.begin_export)
        self.export_btn.pack(side=tk.LEFT)
        self.cancel_btn = ttk.Button(self.action_frame, text='Cancel', command=self.cancel_export)
        self.cancel_btn.pack()
        self.action_frame.grid(row=8, sticky='e', padx=5, pady=5)
        
        self.select_format()

    def unknown_progress(self, start=True, thread=True):
        if thread:
            self.after(1, lambda:self.unknown_progress(start, False))
            return

        if start:
            self.progress.config(mode='indeterminate')
            self.progress.start()
        else:
            self.progress.stop()
            self.progress.config(mode='determinate')
            self.progress_amt.set(0)

    def update_progress_hack(self):
        # hack for tkinter / windows 7
        percentage = self.perc
        if percentage < 0.0:
            percentage = 0.0
        if percentage > 100.0:
            percentage = 100.0
        self.progress_amt.set(percentage)

    def update_progress2(self, percentage=0):
        return

    def update_progress(self, percentage=0):
        with self.lock:
            self.perc = percentage
            self.after(1, self.update_progress_hack)

    def log_error(self, title, msg):
        self.after(50, lambda:tkMessageBox.showerror(title,msg))

    def begin_export(self):
        if self.thread is not None:
            self.export_btn.config(state=tk.DISABLED)
            return
        if not self.validate_form():
            tkMessageBox.showerror("Error", "Invalid export settings")
            return
        if self.sel_module is None:
            tkMessageBox.showerror("Error", "No formatter selected")
            return

        # Begin export here in a separate thread
        data = self.serialise()
        formatter = self.sel_module.Formatter(data['exportas'], data['options'])
        opts = self.app, self, self.database
        if data['tab'] == 0:
            options = []
            if data['simple']['currency'] != '*':
                tag = pykrete.Tag('Currency__')
                val = pykrete.Value(data['simple']['currency'])
                options += [tag == val]
            time_s = self.get_mtgox_query(True)
            time_e = self.get_mtgox_query(False)
            if time_s is not None:
                options += [time_s]
            if time_e is not None:
                options += [time_e]
            if data['simple']['include'] == 0:
                tag = pykrete.Tag('Primary')
                val = pykrete.Value('true')
                options += [tag == val]

            try:
                latest = formatter.latest()
            except IOError as e:
                tkMessageBox.showerror("Export IO Error", e.message)
                return

            if latest is not None:
                tag, val = latest
                tag = pykrete.Tag(tag)
                val = pykrete.Value(val)
                options += [tag > val]

            if len(options) == 0:
                options = ''
            else:
                options = reduce(operator.and_, options)
                options = pykrete.Query('WHERE', options)
            
            tag_mt = pykrete.Query(pykrete.Tag('Money_Trade__'), 'ASC')
            tag_pr = pykrete.Query(pykrete.Tag('Primary'), 'DESC')
            tag_cu = pykrete.Query(pykrete.Tag('Currency__'), 'ASC')
            order = pykrete.Query('ORDER BY', [tag_mt, tag_pr, tag_cu])

            query = pykrete.Query("SELECT * FROM", pykrete.Tag('dump'), options, order)
            query_c = pykrete.Query("SELECT COUNT(*) FROM", pykrete.Tag('dump'), options, order)
            opts += query, formatter, True, query_c
        else:
            opts += data['advanced'], formatter

        try:
            self.thread = Exporter()
            self.thread.setup(*opts)
            self.thread.start()
            self.export_btn.config(state=tk.DISABLED)
        except IOError:
            return

    def cancel_export(self, quit=False):
        quit = quit is True
        if self.thread is not None:
            self.cancel_btn.config(state=tk.DISABLED)
            self.thread.cancel(quit=quit)
        else:
            self.safe_close(quit=quit)

    def finish_export(self, cancelled=False, thread=True):
        if thread:
            self.after(1, lambda:self.finish_export(cancelled, False))
            return

        self.export_btn.config(state=tk.NORMAL)
        self.cancel_btn.config(state=tk.NORMAL)
        self.update_progress()
        self.unknown_progress(False, False)
        self.thread = None
        if cancelled is False:
            self.safe_close()

    def export_as(self, *e):
        opts = {} if self.recc_ext is None else {'defaultextension':self.recc_ext}
        new = tkFileDialog.asksaveasfilename(parent=self, **opts).strip()
        if new != "":
            self.xfile_value.set(new)

    def simp_help(self, *e):
        global simple_help
        tkMessageBox.showinfo("Simple data range help", simple_help)

    def configure_formatters(self):
        modules = formatters.__all__
        mod2name = {}
        name2mod = {}
        for module in modules:
            mod = getattr(formatters, module)
            custom = mod.name if hasattr(mod, 'name') else None
            custom = module if custom is None else custom
            mod2name[module] = custom
            name2mod[custom] = module

        self.formatters_mod2name = mod2name
        self.formatters_name2mod = name2mod
        self.format_sel.config(values=sorted(name2mod.keys()))

    def validate_form(self):
        data = self.serialise()
        valid = True
        if data['tab'] == 0:
            valid = valid and self.validate_date(data['simple']['start'])
            valid = valid and self.validate_date(data['simple']['end'])
        valid = valid and data['exportas']!=""
        valid = valid and os.access(os.path.dirname(data['exportas']), os.W_OK)
        if self.sel_module is not None:
            if hasattr(self.sel_module, 'validate'):
                valid = valid and self.sel_module.validate(data['options'])
        return valid

    def validate_date(self, value):
        if self.get_date(True) > self.get_date(False):
            return False
        value = value.strip()
        if value == '':
            return True
        try:
            dt_parse(value)
        except ValueError:
            try:
                datetime.utcfromtimestamp(float(value))
            except ValueError:
                return False
        return True

    def get_date(self, start=True):
        widget = self.simp_start if start else self.simp_end
        value = widget.get().strip()
        default = float('-inf') if start else float('+inf')
        if value == '':
            return default
        else:
            try:
                parsed = dt_parse(value)
            except ValueError:
                try:
                    parsed = datetime.utcfromtimestamp(float(value))
                except ValueError:
                    return default
            if parsed.tzinfo is not None:
                offset = parsed.tzinfo.utcoffset(parsed)
                parsed -= offset
            micro = parsed.microsecond
            return timegm(parsed.timetuple())*1000000 + micro

    def get_mtgox_query(self, start=True):
        stamp = self.get_date(start)
        if math.isinf(stamp):
            return None
        jump = 1309000000000000
        if stamp < jump:
            dt = datetime.utcfromtimestamp(stamp)
            val = pykrete.Value(dt.strftime('%Y-%m-%d %H:%M:%S'))
            tag = pykrete.Tag('Date')
        else:
            val = pykrete.Value(stamp)
            tag = pykrete.Tag('Money_Trade__')
        return tag >= val if start else tag <= val

    def foc_flash(self, widget, count=15):
        widget.focus_force()
        if count < 1:
                widget.selection_range(0, tk.END)
        else:
            if count%3 == 0:
                widget.bell()
                widget.selection_range(0, tk.END)
            else:
                widget.selection_range(0, 0)
            self.after(25, lambda:self.foc_flash(widget, count-1))

    def invalid_entry(self, name, orig=''):
        widget = self.nametowidget(name)
        widget.focus_set()
        widget.bell()
        self.foc_flash(widget)

    def show_schema(self, *e):
        global db_schema
        help_url = 'https://bitcointalk.org/index.php?topic=218980.msg2303880#msg2303880'
        resp = tkMessageBox.askyesno("Dump DB Schema", db_schema + "\n\nbit.ly/1cUGcld")
        if resp:
            webbrowser.open(help_url, new=1, autoraise=True)

    def select_format(self, *e):
        option = self.format_sel.get()
        try:
            module = getattr(formatters, self.formatters_name2mod[option])
            frame = module.Widget(self.format_frame)
            frame.init_gui()
            if hasattr(module, 'ext') and module.ext is not None:
                self.recc_ext = module.ext
            else:
                self.recc_ext = None
            self.sel_module = module
        except (AttributeError, KeyError):
            self.recc_ext = None
            self.sel_module = None
            frame = ttk.LabelFrame(self.format_frame)

        self.options_frame.grid_forget()
        self.options_frame = frame
        lw = tk.Frame(self.options_frame, relief='flat', width=309)
        self.options_frame.config(labelwidget=lw)
        self.options_frame.grid(row=2, columnspan=3)

    serial_version = '1.0'

    def serialise(self):
        option = self.format_sel.get().strip()
        module = self.formatters_name2mod[option] if option != "" else ""
        try:
            options = self.options_frame.serialise()
        except AttributeError:
            options = None

        return {
            'version':self.serial_version,
            'tab':self.data_sel.index(self.data_sel.select()),
            'simple':{
                'currency':self.simp_currencies.get(),
                'start':self.simp_start.get().strip(),
                'end':self.simp_end.get().strip(),
                'include':self.simp_primary_value.get()
            },
            'advanced':self.adv_sql.get(1.0, tk.END).strip(),
            'format':module,
            'exportas':self.xfile_value.get().strip(),
            'options':options
        }

    # use json instead of pickle as less risky, and we're only storing a dictionary of strings and ints

    def load_prefs(self, *e):
        fp = tkFileDialog.askopenfile(mode='r', defaultextension='json', parent=self)
        if fp is None:
            return
        try:
            data = json.load(fp)
        except ValueError:
            fp.close()
            tkMessageBox.showerror("Error", "Invalid preferences file")
            return
        fp.close()

        if data['version'] != self.serial_version:
            pass
        self.data_sel.select(data['tab'])
        self.simp_currencies.set(data['simple']['currency'])
        replace_text(self.simp_start, data['simple']['start'], 0)
        replace_text(self.simp_end, data['simple']['end'], 0)
        self.simp_primary_value.set(data['simple']['include'])
        replace_text(self.adv_sql, data['advanced'])

        module = data['format'].strip()
        try:
            option = self.formatters_mod2name[module] if module != "" else ""
        except KeyError:
            tkMessageBox.showwarning("Warning", "The formatter specified in the export preferences file is no longer available.")
            option = ""
        self.format_sel.set(option)

        self.xfile_value.set(data['exportas'])
        if data['format'].strip() != '':
            self.select_format()

        options = data['options']
        try:
            self.options_frame.deserialise(options)
        except:
            pass

    def save_prefs(self, *e):
        fp = tkFileDialog.asksaveasfile(mode='w', defaultextension='json', initialfile='mtgox-export-prefs.json', parent=self)
        if fp is None:
            return
        json.dump(self.serialise(), fp)
        fp.close()

    def safe_close(self, *e, **opts):
        self.destroy()
        self.on_close()
        if 'callback' in opts:
            opts['callback']()
        if self.app is not None and 'quit' in opts and opts['quit']:
            self.app.safe_quit()