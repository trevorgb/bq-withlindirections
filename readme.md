MtGox-Trades-Tool
===

Tips and donations: `13P5TsDvSdCZ9KnYv6RuNyvrAZ54Mrfy9k`

---

This tool will allow you to download, update and maintain a local copy of MtGox's Google BigQuery trade database, and use it to export trade data to a variety of formats, as well as updating these exported files.

Obtaining
---

If you are on Windows or Mac, you can download a pre-compiled binary from the downloads section, in addition to an in-depth readme with detailed instructions on using the program.

If you want to run the latest development version, you should checkout this git repository, and then download the dependencies from the downloads section into the same folder. You can then run the app using `python app.py`. You can also build a binary using `python setup.py py2exe` for Windows, or `python setup.py py2app` for Mac. Linux users may need to install the python tkinter modules manually. Please note that this tool is untested on Linux.

If you are on Debian Linux, see readme.LINUX

Quickstart
---

Download Readme.pdf from the downloads section for more detailed information.

### Authenticating
When you first run the tool, you will be asked to authenticate with Google. To complete this, you need a Google account (it doesn't need to be registered with BigQuery to work, unlike previous versions of this tool). A webbrowser should be opened for you (if not, click Copy URL and paste it into your browser's address bar). Make sure you're logged into Google, then click accept. You will now receive an authorisation code, which you should copy to the tool's code entry. Click Complete Authentication and then you should be ready to go. You may need to do this process again in the future if your authorisation token expires.

### Dumps
To create a new dump, go to File, New Dump... You can load a previous dump using File, Load Dump... You can then download the dump using the Update button. You can safely cancel using the Cancel button (this may take a few moments), and then resume at any point with the Update button. Use the quit button to exit safely (after cancelling, if necessary).

### Exports
Until MtGox has started their automatic updates, and it has been confirmed that the table is presorted, you should always make sure that your dump is complete before exporting. When you are ready to export, go File, Export As...

From the export window, you can select the data range you want to export. 

* The simple tab allows you to choose which currency to export (or all of them, *), the start and end date periods (using Unix timestamp or ISO 8601 format), and whether or not to include primary trades. If you leave any of the date entries blank then the range will be unbounded on that side (e.g. leave the End blank to export all data after start, or both blank to export all data).
* The advanced tab allows you to enter a custom SQL query (make sure it's valid in the SQLite3 dialect). Click the Database Schema button for information on the table (it is called `dump`), and the fields schema. You should usually select all fields with * (behaviour is undefined if you do not select all the columns in the database), sort by `Money_Trade__ ASC,[Primary] DESC,Currency__ ASC`, and omit the trailing semicolon (the tool will modify your query slightly in order to determine how many rows your query generates to update your progress). Note that the database is not read-only, so be careful not to change the database in any way.

You can then select what format you want to export as, and where to export. You can update a prior export by selecting an existing file in the Browse... window. Please note that although your OS may warn you of replacing or overwriting this file, the tool will not erase it, it will only append to it.

If there are any options for the export format you selected, then customise them as you want.

You probably want to save your export preferences so that you can update the export in the future (if you use different settings than last time this may not be detected and you may corrupt your export or make it invalid). You can do this with the Save button at the top of the window. You can then reload these using the Load button.

Finally, click export to begin the export. You should be able to safely cancel at any time with the Cancel button. This will not delete the export, it merely pauses the export so that it can be continued at a later time. When the export is complete, the window will close. If you update the dump in the future, you can reload your export settings and click Export to bring it up to date.

Note that the export progress bar is currently disabled under Windows because the ttk module crashes on recent versions of Windows. It is still present on Mac. I hope in the future to resolve this problem. In the meantime, it is still ok to export, but you will have no indication of how much longer there is to go.

Bugs
---
If you encounter a bug or a crash, please report it at the forum thread [here](https://bitcointalk.org/index.php?topic=221055.0).

Discussion
---
<https://bitcointalk.org/index.php?topic=221055.0>

MtGox BigQuery Information
---
<http://bitcointalk.org/index.php?topic=218980.0>
