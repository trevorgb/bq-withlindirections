#!/usr/bin/python
"""

	Copyright (c) 2013 William Earley

    This file is part of MtGox Trades Tool (https://bitbucket.org/nitrous/bq).

    MtGox Trades Tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MtGox Trades Tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MtGox Trades Tool.  If not, see <http://www.gnu.org/licenses/>.

"""

"""

Layout:

def dump(...):
	This is for the first dump
	Use bigquery to download the table efficiently,
		even though it's unsorted, if we start at 0
		then we should be able to dump the entire
		table even if it's changed midway
	Warning, if the app crashes, the dump is useless
	Write rows to basic database, maybe even flat file

def process(...):
	heapsort the dump into desired format

def update(...):
	select count(1) from dump
	bq.table(mt-gox,mtgox,trades, _)
	insert (sort _) into dump

Basically, use dump to create the initial download
(heap-)Sort and insert into desired format
Then, periodically use update to maintain it

heap sort is used for the initial dump because it will
be v. large, sort for update can be any, although I guess
it might as well still be heapsort

This mechanism assumes MtGox uses WRITE_APPEND and that
Google "don't be evil", however it would be able to even
produce a sorted csv file without incurring any data limits
to your bigquery account


Field	        	Description	Type
Money_Trade__		This is the tid value, a unique id for the order. The first trades used (almost) sequential integers up to 218868, the majority of trades however came after this (~6 million) and use a Unix microstamp instead.	int
Primary				Whether the trade happened in the currency specified (see Currency__). Some trades may appear on multiple tickers, so to ignore duplicates you may want to `select where primary is true'.	bool
Currency__			The symbol of the currency against which BTC was bought or sold.	str
Bid_User_Rest_App__	App ID of the bidding party (the person buying BTC)*	str
Ask_User_Rest_App__	App ID of the asking party (the person selling BTC)*	str
Type				bid or ask (bid means to buy BTC by selling the specified currency, ask means to buy the specified currency by selling BTC)	str
Properties			A comma separated list of properties of the order, e.g. limit,market,mixed_currency	str
Item				The thing being bought or sold, always BTC.	str
Amount				The volume of the item (BTC) being bought/sold in satoshis (1 BTC = 100,000,000 satoshis).	int
Price				The price of each item in the currency specified. For most currencies, this is given in units of 0.00001 of the currency, so $12.3456 would be 1,234,560. Some currencies, currently JPY and SEK use units of 0.001 instead.	int
Date				The date of the trade in ISO format.	str

"""

from pykrete import DB, Query, Tag, Value, getdict
import sqlite3
import threading
import time

def db_index(db):
	tag_mt = Query(Tag('Money_Trade__'), 'ASC')
	tag_pr = Query(Tag('Primary'), 'DESC')
	tag_cu = Query(Tag('Currency__'), 'ASC')
	cols = Query([tag_mt, tag_pr, tag_cu]).enclose()
	query = Query('CREATE UNIQUE INDEX IF NOT EXISTS', Tag('trade_index'), 'ON', Tag('dump'), cols)
	db(query)

def db_last(db):
	tag_mt = Query(Tag('Money_Trade__'), 'DESC')
	tag_pr = Query(Tag('Primary'), 'ASC')
	tag_cu = Query(Tag('Currency__'), 'DESC')
	order = Query('ORDER BY', [tag_mt, tag_pr, tag_cu])
	query = Query('SELECT * FROM', Tag('dump'), order, 'LIMIT 1')
	return getdict(db(query).fetchone())

class Updater(threading.Thread):
	table_id = ['mt-gox', 'mtgox', 'trades']
	def cancel(self, quit=False):
		with self.lock:
			self.cancelled = True
			self.quit = quit is True

	def setup(self, app, bqo, database, callback=lambda *x:None):
		db = DB(database)
		db('PRAGMA legacy_file_format = OFF;')
		if 'dump' not in db:
			db['dump'] = {
				'Money_Trade__':int,
				'Primary':int,
				'Currency__':str,
				'Bid_User_Rest_App__':str,
				'Ask_User_Rest_App__':str,
				'Type':str,
				'Properties':str,
				'Item':str,
				'Amount':int,
				'Price':int,
				'Date':str
			}
		db.close()

		self.app = app
		self.db = database
		self.cancelled = False
		self.lock = threading.RLock()
		self.bqo = bqo
		self.quit = False
		self.callback=callback

	def run(self):
		db = DB(self.db)
		table = db['dump']

		app = self.app
		bqo = self.bqo
		orig = pos = len(table)
		values = bqo.table(*(self.table_id + [pos]))
		dlo = bqo.length(*self.table_id)
		dl = dlo - orig

		app.log_output("Update in progress - %d rows to download" % dl)
		db_index(db) #can take a few minutes if creating on a full db!

		for page in values['pages']:
			table[...] = page
			pos += len(page)
			size = len(table)

			last = page[-1]
			rows = "%d / %d"%(size,dlo) if size != dlo else size
			self.callback(rows, last['Money_Trade__'], last['Date'])
			
			if pos != size:
				app.log_error("Database insertion error, %d =/= %d" % (pos,size))
				raise Exception, "Insertion error: %d =/= %d" % (pos,size)
			progress = 100.0 * (pos - orig) / dl
			app.update_progress(progress)
			#app.log_output("%d rows in DB, %d to go. Update %0.2f%% complete" % (pos,dlo-pos,progress))
			with self.lock:
				if self.cancelled:
					app.log_output("Cancelled")
					break

		with self.lock:
			if not self.cancelled:
				app.log_output("Update complete")

		db.close()
		app.finish_update()
		if self.quit:
			app.safe_quit()

class Exporter(threading.Thread):
	def setup(self, app, win, database, query, formatter, override_latest=False, override_count=None):
		self.app = app
		self.win = win
		self.database = database
		self.formatter = formatter

		query_b = None
		if not override_latest:
			try:
				latest = formatter.latest()
			except IOError as e:
				self.win.log_error("Export IO Error", e.message)
				self.win.finish_export(True)
				raise

			if latest is not None:
				query_b = Query("FROM (%s) WHERE" % query, Tag(latest[0])>Value(latest[1]))
				query = Query("SELECT *", query_b)
		self.query = query

		if override_count is not None:
			self.query_c = override_count
		elif query_b is not None:
			self.query_c = Query("SELECT COUNT(*)", query_b)
		else:
			self.query_c = "SELECT COUNT(*) FROM (%s)" % query

		self.cancelled = False
		self.lock = threading.RLock()
		self.quit = False

	def run(self):
		self.win.unknown_progress()
		self.db = DB(self.database)

		try:
			size = self.db(self.query_c).fetchone()[0]
			self.app.log_output("Export in progress - %d rows to process" % size)
			results = self.db(self.query)
		except sqlite3.OperationalError as e:
			self.win.log_error("SQL Query Error", e.message)
			self.app.log_output("Export Error!")
			self.win.finish_export(True)
			return

		db_index(self.db) #can take a few minutes if creating on a full db!
		i = 0

		last = db_last(self.db)
		self.formatter.prepare(size, last)
		try:
			self.formatter.open()
		except IOError as e:
			self.win.log_error("Export IO Error", e.message)
			self.app.log_output("Export Error!")
			self.win.finish_export(True)
			return

		self.win.unknown_progress(False)
		t0 = time.time()
		for result in results:
			with self.lock:
				if self.cancelled:
					self.app.log_output("Cancelled")
					break
			self.formatter.write(i, getdict(result))

			i += 1		
			# tkinter is broken on windows 7, hacky fix
			# so tk doesn't get confused (hopefully)
			if i % 25 == 0:
				t1 = time.time()
				if t1 - t0 > 0.03:
					self.win.update_progress(100.0 * i / size)
					t0 = t1


		with self.lock:
			if not self.cancelled:
				self.app.log_output("Export complete")

		self.db.close()
		self.formatter.close()
		self.win.finish_export(self.cancelled)
		if self.quit:
			self.app.safe_quit()

	def cancel(self, quit=False):
		with self.lock:
			self.cancelled = True
			self.quit = quit is True